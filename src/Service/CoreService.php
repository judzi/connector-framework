<?php

namespace App\Service;

class CoreService
{
    public function find(string $line): array
    {
        $file = '/application/public/frmw-test-file01.csv';
        $line++;
        $content = exec(sprintf('sed -n %dp %s', $line, $file));
        if (empty($content)) {
            return ['error' => 'Unable to find data'];
        }
        $contentParts = explode(';', $content);

        return [
            'value_x' => $contentParts[1],
            'value_y' => $contentParts[2],
            'value_z' => $contentParts[3],
        ];
    }
}