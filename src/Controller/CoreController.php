<?php

namespace App\Controller;

use App\Service\CoreService;
use Symfony\Component\HttpFoundation\JsonResponse;

class CoreController
{
    public function find(string $line, CoreService $service): JsonResponse
    {
        $data = $service->find($line);
        return new JsonResponse($data, isset($data['error']) ? 400 : 200);
    }
}