## Image
- Image registry URL: `registry.gitlab.com/judzi/connector-framework/php:latest`
- Image size (registry): `32.92 MiB`
- Image size (locally): `94.4 MiB`

[![pipeline status](https://gitlab.com/judzi/connector-framework/badges/master/pipeline.svg)](https://gitlab.com/judzi/connector-framework/-/commits/master)

## Testing

### Endpoint
```
GET https://juraj-test-framework.adfinance.cz/find/{lineNum}
```
Example response
```json
{"value_x":"44184369","value_y":"50272263","value_z":"3835731"}
```

### Test command
```bash
$ wrk -d60s --latency -s generate_int.lua https://juraj-test-framework.adfinance.cz
```

### Results
```bash
$ wrk -d60s --latency -s generate_int.lua https://juraj-test-framework.adfinance.cz

Running 1m test @ https://juraj-test-framework.adfinance.cz
  2 threads and 10 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   136.17ms   15.22ms 209.92ms   72.27%
    Req/Sec    36.37      9.26    50.00     70.20%
  Latency Distribution
     50%  134.32ms
     75%  145.12ms
     90%  155.92ms
     99%  180.35ms
  4374 requests in 1.00m, 1.52MB read
Requests/sec:     72.78
Transfer/sec:     25.92KB
```

## Packages
- Using version *5.0* for 
    - `symfony/config`
    - `symfony/http-kernel`
    - `symfony/http-foundation`
    - `symfony/routing`
    - `symfony/dependency-injection`
    - `symfony/framework-bundle`

## Setup 
```bash
docker run -d --restart always --name juraj-test-framework-php --net nginx-proxy registry.gitlab.com/judzi/connector-framework/php:latest
```
```bash
docker run -d --restart always --name juraj-test-framework-nginx -e VIRTUAL_HOST=juraj-test-framework.adfinance.cz -e VIRTUAL_PORT=80 -e LETSENCRYPT_HOST=juraj-test-framework.adfinance.cz -e LETSENCRYPT_EMAIL=dev@usetreno.cz -e PHP_NAME=juraj-test-framework-php --net nginx-proxy gitlab.adfinance.cz:4567/renat-magadiev/template-competition/nginx:ecbc26356edda6be071855bdcd9dec376d989869
```